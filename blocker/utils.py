import functools
import re
import typing

import discord
from discord.ext import commands


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class BlockerError(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message

    def __str__(self):
        return ':triangular_flag_on_post: ' + self.message


class MemberConverter:
    __member: discord.Member

    def __init__(self, member: discord.Member):
        self.__member = member

    def __call__(self):
        return self.__member

    @classmethod
    def __get_by_id(cls, ctx: commands.Context, name: str):
        match = re.match(r'([0-9]{15,21})$', name)
        if match is not None:
            user_id = int(match.group(1))
            return ctx.guild.get_member(user_id)
        else:
            return None

    @classmethod
    def __get_by_mention(cls, ctx: commands.Context, name: str) -> typing.Optional[discord.Member]:
        match = re.match(r'<@!?([0-9]+)>$', name)
        if match is not None:
            user_id = int(match.group(1))
            return ctx.guild.get_member(user_id)
        else:
            return None

    @classmethod
    def __get_by_discriminator(cls, ctx: commands.Context, name: str) -> typing.Optional[discord.Member]:
        if len(name) > 5 and name[-5] == '#':
            potential_discriminator = name[-4:]
            result = discord.utils.get(ctx.guild.members, name=name[:-5], discriminator=potential_discriminator)
            if result is not None:
                return result
        else:
            return None

    @classmethod
    def __get_by_partial_name(cls, ctx: commands.Context, name: str) -> typing.Optional[discord.Member]:
        name = name.lower()
        for m in ctx.guild.members:
            m_nick = m.nick.lower() if m.nick is not None else str()
            m_name = m.name.lower()
            if name in m_nick or name in m_name: return m
        return None

    @classmethod
    async def convert(cls, ctx: commands.Context, argument: str) -> 'MemberConverter':
        result = cls.__get_by_id(ctx, argument)
        if result is not None: return MemberConverter(result)
        result = cls.__get_by_mention(ctx, argument)
        if result is not None: return MemberConverter(result)
        result = cls.__get_by_discriminator(ctx, argument)
        if result is not None: return MemberConverter(result)
        result = cls.__get_by_partial_name(ctx, argument)
        if result is not None: return MemberConverter(result)
        raise commands.BadArgument('ПОльзователь {} не найден'.format(argument))


class IdManageable:
    __id: int

    def __init__(self, *ids: int):
        self.__id = functools.reduce(lambda a, b: (a << 64) + b, reversed(ids))

    def __int__(self) -> int:
        return self.__id

    def __getitem__(self, key: int) -> int:
        return (self.__id >> (64 * key)) & 0xffffffffffffffff


class PermissionChecker:
    BAN_PERMS = ('ban_members',)
    KICK_PERMS = ('kick_members',)
    MUTE_PERMS = ('manage_roles',)
    TUNE_USER_PERMS = ('manage_guild',)

    @staticmethod
    def compare(m1: discord.Member, m2: discord.Member) -> int:
        if m1.guild != m2.guild: raise RuntimeError('Сравнение некорректно')
        if m1 == m2: return 0
        if m1 == m1.guild.owner: return 1
        if m2 == m2.guild.owner: return -1
        if m1.top_role > m2.top_role: return 1
        if m2.top_role > m1.top_role: return -1
        return 0

    @staticmethod
    def has_all_perms(member: discord.Member, perms: typing.Tuple[str, ...]):
        return all(getattr(member.guild_permissions, perm, False) for perm in perms)

    @classmethod
    def is_able_to_ban(cls, member: discord.Member) -> bool:
        return cls.has_all_perms(member, cls.BAN_PERMS)

    @classmethod
    def is_able_to_kick(cls, member: discord.Member):
        return cls.has_all_perms(member, cls.KICK_PERMS)

    @classmethod
    def is_able_to_mute(cls, member: discord.Member):
        return cls.has_all_perms(member, cls.MUTE_PERMS)

    @classmethod
    def is_able_to_tune(cls, member: discord.Member, is_bot: bool = False):
        return is_bot or cls.has_all_perms(member, cls.TUNE_USER_PERMS)
