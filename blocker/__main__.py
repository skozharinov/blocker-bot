import argparse
import asyncio
import logging

from blocker import bot, db_client
from blocker.cogs import base_cog, warn_cog, mute_cog, settings_cog

logging.basicConfig(level=logging.INFO)

parser = argparse.ArgumentParser(description='Бот-модератор для Discord')
parser.add_argument('-D', '--discord-token', action='store', dest='discord_token', help='Токен Discord')
parser.add_argument('-M', '--mongo-url', action='store', dest='mongo_url', help='MongoDB URL')
args = parser.parse_args()

loop = asyncio.get_event_loop()
db_client = db_client.DbClient(args.mongo_url)

blocker = bot.Bot(command_prefix='!', description='Бот-модератор')
blocker.add_cog(settings_cog.SettingsCog(blocker))
blocker.add_cog(base_cog.BaseCog(blocker))
blocker.add_cog(mute_cog.MuteCog(blocker))
blocker.add_cog(warn_cog.WarnCog(blocker))
blocker.run(args.discord_token)
