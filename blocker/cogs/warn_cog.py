import asyncio
import datetime
import typing

import discord
from discord.ext import commands

from blocker import db_client, utils, bot


class Warn(utils.IdManageable):
    count: int
    last: datetime.datetime

    def __init__(self, *, member: discord.Member = None, guild_id: int = None, user_id: int = None,
                 count: int = 0, last: datetime.datetime = datetime.datetime.now()):
        if member is not None:
            if guild_id is not None or user_id is not None:
                raise RuntimeError('Неправильная инициализация класса Warn')

            super().__init__(member.guild.id, member.id)
        else:
            if guild_id is None or user_id is None:
                raise RuntimeError('Неправильная инициализация класса Warn')

            super().__init__(guild_id, user_id)

        self.count = count
        self.last = last

    def increment(self):
        self.count += 1
        self.last = datetime.datetime.now()

    @property
    def guild_id(self) -> int:
        return self[0]

    @property
    def user_id(self) -> int:
        return self[1]

    @property
    def left(self) -> int:
        return self.required - self.count

    @property
    def required(self) -> int:
        return bot.Bot().get_cog('SettingsCog')[self.guild_id].warns_limit

    @property
    def is_over_limit(self) -> bool:
        return self.left <= 0

    async def clear(self):
        self.count = 0
        self.last = datetime.datetime.utcfromtimestamp(0)
        await self.save()

    async def save(self):
        search = {'user_id': self.user_id, 'guild_id': self.guild_id}
        update = {'count': self.count, 'last': self.last}
        await db_client.DbClient().get_database()['warns'].update_one(search, {'$set': update}, upsert=True)


class WarnCog(commands.Cog):
    __warns: typing.List[Warn]
    bot: commands.Bot

    def __init__(self, bot_: commands.Bot):
        self.bot = bot_
        self.__warns = []
        asyncio.ensure_future(self.load())

    @commands.command(aliases=['знак'])
    @commands.guild_only()
    async def warn(self, ctx: commands.Context, member: utils.MemberConverter):
        """Выдаёт предупреждение пользователю.
        member - ID, упоминание или никнейм (если содержит пробелы, должен быть заключен в кавычки).
        Необходимо разрешение для выбранного действия (см. команду set) для бота и вызывающего команду."""

        await self.do_warn(ctx, member())

    async def do_warn(self, ctx: commands.Context, member: discord.Member):
        settings = self.bot.get_cog('SettingsCog')[member.guild]

        if not settings.warn_action.is_able_to_warn(ctx.author, member):
            raise commands.MissingPermissions([])

        if not settings.warn_action.is_able_to_warn(ctx.me, member):
            raise commands.BotMissingPermissions([])

        warn = self.__get_warn(member)
        warn.increment()

        if warn.is_over_limit:
            await warn.clear()
            await settings.action_on_warn(member)
            await ctx.send(':yum: Лимит предупреждений пользователя {} исчерпан'.format(member))
        else:
            await warn.save()
            await ctx.send(':grimacing: Предупреждение пользователю {}, осталось {:d}'.format(member, warn.left))

    async def load(self):
        async for d in db_client.DbClient().get_database()['warns'].find():
            guild_id, user_id = d['guild_id'], d['user_id']
            count, last = d['count'], d['last']
            self.__warns.append(Warn(guild_id=guild_id, user_id=user_id, count=count, last=last))

    def __get_warn(self, member: discord.Member) -> Warn:
        warn = next(filter(lambda w: w.guild_id == member.guild.id and w.user_id == member.id, self.__warns), None)
        if warn is None:
            warn = Warn(member=member)
            self.__warns.append(warn)
        return warn
