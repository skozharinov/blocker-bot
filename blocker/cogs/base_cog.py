from discord.ext import commands

from blocker import utils


class BaseCog(commands.Cog):
    bot: commands.Bot

    def __init__(self, bot: commands.Bot):
        self.bot = bot

    @commands.command(aliases=['бан'])
    @commands.guild_only()
    async def ban(self, ctx: commands.Context, member: utils.MemberConverter, reason: str = None):
        """Банит указанного пользователя на сервере.
        member - ID, упоминание или никнейм (если содержит пробелы, должен быть заключен в кавычки).
        reason - причина бана.
        Необходимо разрешение бвнить пользователей для бота и вызывающего команду."""

        if utils.PermissionChecker.compare(ctx.author, member()) <= 0 \
                or not utils.PermissionChecker.is_able_to_ban(ctx.author):
            raise commands.MissingPermissions(list(utils.PermissionChecker.BAN_PERMS))

        if utils.PermissionChecker.compare(ctx.me, member()) <= 0 \
                or not utils.PermissionChecker.is_able_to_ban(ctx.me):
            raise commands.BotMissingPermissions(list(utils.PermissionChecker.BAN_PERMS))

        await member().ban(reason=reason)
        await ctx.send(':oncoming_police_car: Забанен пользователь {}'.format(member))

    @commands.command(aliases=['кик'])
    @commands.guild_only()
    async def kick(self, ctx: commands.Context, member: utils.MemberConverter, reason: str = None):
        """Выгоняет пользователя.
        member - ID, упоминание или никнейм (если содержит пробелы, должен быть заключен в кавычки).
        reason - причина кика.
        Необходимо разрешение кикать пользователей для бота и вызывающего команду."""

        if utils.PermissionChecker.compare(ctx.author, member()) <= 0 \
                or not utils.PermissionChecker.is_able_to_kick(ctx.author):
            raise commands.MissingPermissions(list(utils.PermissionChecker.KICK_PERMS))

        if utils.PermissionChecker.compare(ctx.me, member()) <= 0 \
                or not utils.PermissionChecker.is_able_to_kick(ctx.me):
            raise commands.BotMissingPermissions(list(utils.PermissionChecker.KICK_PERMS))

        await member().kick(reason=reason)
        await ctx.send(':oncoming_police_car: Выгнан пользователь {}'.format(member))
