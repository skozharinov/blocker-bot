import asyncio
import enum
import typing

import discord
from discord.ext import commands

from blocker import utils, db_client


class WarnLimitAction(enum.Enum):
    Ban = enum.auto()
    Kick = enum.auto()

    def is_able_to_warn(self, req: discord.Member, victim: discord.Member) -> bool:
        if utils.PermissionChecker.compare(req, victim) <= 0: return False
        if self == self.Kick and not utils.PermissionChecker.is_able_to_kick(req): return False
        if self == self.Ban and not utils.PermissionChecker.is_able_to_ban(req): return False
        return True

    async def __call__(self, victim: discord.Member):
        if self == self.Kick: await victim.kick()
        if self == self.Ban: await victim.ban()


class Settings(utils.IdManageable):
    warn_action: WarnLimitAction
    warns_limit: int
    muted_role: str

    def __init__(self, *, guild: discord.Guild = None, guild_id: int = None,
                 warn_action: WarnLimitAction = WarnLimitAction.Ban, warns_limit: int = 3, muted_role: str = 'Мут'):
        if guild is not None:
            if guild_id is not None:
                raise RuntimeError('Неправильная инициализация класса Settings')

            super().__init__(guild.id)
        else:
            if guild_id is None:
                raise RuntimeError('Неправильная инициализация класса Settings')

            super().__init__(guild_id)

        self.warn_action = warn_action
        self.warns_limit = warns_limit
        self.muted_role = muted_role

    @property
    def guild_id(self) -> int:
        return self[0]

    async def save(self):
        search = {'guild_id': self.guild_id}
        update = {'warn_action': self.warn_action.name, 'warns_limit': self.warns_limit, 'muted_role': self.muted_role}
        await db_client.DbClient().get_database()['settings'].update_one(search, {'$set': update}, upsert=True)


class SettingsCog(commands.Cog):
    __settings: typing.List[Settings]
    bot: commands.Bot

    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.__settings = []
        asyncio.ensure_future(self.load())

    def __getitem__(self, guild_id: typing.Union[int, discord.Guild]) -> Settings:
        if isinstance(guild_id, discord.Guild):
            guild_id = guild_id.id

        settings = next(filter(lambda s: s.guild_id == guild_id, self.__settings), None)

        if settings is None:
            settings = Settings(guild_id=guild_id)
            self.__settings.append(settings)

        return settings

    @commands.command(aliases=['задать'])
    @commands.guild_only()
    async def set(self, ctx: commands.Context, prop: str = None, val: str = None):
        """Управляет настройками бота.
        prop - имя параметра (см. ниже), если не указано, будут выведены все настройки сервера.
        val - значение параметра, если не указано, будет выведено текущее значение этого параметра.
        Необходимо разрешение управлять сервером для вызывающего команду.
        warnslim/знакилим - Лимит предупреждений [число] (по умолчанию - 3).
        warnact/знакдей - Действие по достижению лимита предупреждений [ban/бан/kick/кик] (по умолчанию - бан).
        mutrole/мутроль - Роль, используемая для управления мутом (по умолчанию - Мут)."""

        if not utils.PermissionChecker.is_able_to_tune(ctx.author, False):
            raise commands.MissingPermissions(utils.PermissionChecker.TUNE_USER_PERMS)

        settings = self[ctx.guild]

        if prop is None:
            action = {WarnLimitAction.Ban: 'бан', WarnLimitAction.Kick: 'кик'}[settings.warn_action]
            await ctx.send(':point_right: Лимит предупреждений: {:d}, действие: {}, роль для мута: {}'
                           .format(settings.warns_limit, action, settings.muted_role))
            return

        if prop.lower() in ('warnact', 'знакдей'):
            if val in ('ban', 'бан'):
                settings.warn_action = WarnLimitAction.Ban
            elif val in ('kick', 'кик'):
                settings.warn_action = WarnLimitAction.Kick
            elif val is None:
                action = {WarnLimitAction.Ban: 'бан', WarnLimitAction.Kick: 'кик'}[settings.warn_action]
                await ctx.send(':point_right: Действие: {}'.format(action))
                return
            else:
                raise commands.UserInputError()
        elif prop.lower() in ('warnslim', 'знакилим'):
            if val is None:
                await ctx.send(':point_right: Лимит предупреждений: {}'.format(settings.warns_limit))
                return
            elif val.isdigit():
                settings.warns_limit = int(val)
            else:
                raise commands.UserInputError()
        elif prop.lower() in ('mutrole', 'мутроль'):
            if val is None:
                await ctx.send(':point_right: Роль для мута - {}'.format(settings.muted_role))
                return
            else:
                settings.muted_role = val
        else:
            raise commands.UserInputError()

        await settings.save()
        await ctx.channel.send(':ok_hand: Настройки обновлены')

    async def load(self):
        async for d in db_client.DbClient().get_database()['settings'].find():
            guild_id = d['guild_id']
            warn_action, warns_limit, muted_role = WarnLimitAction[d['warn_action']], d['warns_limit'], d['muted_role']
            self.__settings.append(Settings(guild_id=guild_id, warn_action=warn_action,
                                            warns_limit=warns_limit, muted_role=muted_role))
