import asyncio
import datetime
import typing

import discord
from discord.ext import commands

from blocker import utils, db_client


class Mute(utils.IdManageable):
    unmute_delay: datetime.timedelta
    unmute_task: asyncio.Future

    def __init__(self, *, member: discord.Member = None, guild_id: int = None, user_id: int = None,
                 unmute_delay: datetime.timedelta = None, unmute_action: typing.Callable[[int, int], typing.Awaitable]):
        if member is not None:
            if guild_id is not None or user_id is not None:
                raise RuntimeError('Неправильная инициализация класса Mute')

            super().__init__(member.guild.id, member.id)
        else:
            if guild_id is None or user_id is None:
                raise RuntimeError('Неправильная инициализация класса Mute')

            super().__init__(guild_id, user_id)

        self.unmute_delay = unmute_delay
        self.unmute_action = unmute_action
        self.unmute_task = asyncio.ensure_future(self.unmute())

    @property
    def guild_id(self) -> int:
        return self[0]

    @property
    def user_id(self) -> int:
        return self[1]

    async def unmute(self):
        if self.unmute_delay is None: return
        await asyncio.sleep(self.unmute_delay.total_seconds())
        await self.unmute_action(self.guild_id, self.user_id)

    def cancel(self):
        self.unmute_task.cancel()


class MuteCog(commands.Cog):
    __mutes: typing.List[Mute]
    bot: commands.Bot

    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.__mutes = []
        asyncio.ensure_future(self.load())

    def __contains__(self, member: discord.Member) -> bool:
        return not any(mute.user_id == member.id and mute.guild_id == member.guild.id for mute in self.__mutes)

    async def load(self):
        unmute_action = self.do_unmute_by_id

        async for d in db_client.DbClient().get_database()['mutes'].find():
            guild_id, user_id = d['guild_id'], d['user_id']

            if d['unmute_date'] is None:
                unmute_delay = None
            else:
                unmute_delay = d['unmute_date'] - datetime.datetime.now()

                if unmute_delay.total_seconds() < 0:
                    await unmute_action(guild_id, user_id)
                    continue

            self.__mutes.append(Mute(guild_id=guild_id, user_id=user_id,
                                     unmute_delay=unmute_delay, unmute_action=unmute_action))

    @commands.command(aliases=['мут'])
    @commands.guild_only()
    async def mute(self, ctx: commands.Context, member: utils.MemberConverter):
        """Добавляет мут пользователю.
        member - ID, упоминание или никнейм (если содержит пробелы, должен быть заключен в кавычки).
        Необходимо разрешение управлять ролями для бота и вызывающего команду.
        Для мута используется специальная роль (см. команду set).
        Роль создаётся при первом вызове команды mute.
        Для правильной работы она должна быть выше остальных ролей на сервере, кроме бота."""

        if utils.PermissionChecker.compare(ctx.author, member()) <= 0 \
                or not utils.PermissionChecker.is_able_to_mute(ctx.author):
            raise commands.MissingPermissions(list(utils.PermissionChecker.MUTE_PERMS))

        if utils.PermissionChecker.compare(ctx.me, member()) <= 0 \
                or not utils.PermissionChecker.is_able_to_mute(ctx.me):
            raise commands.BotMissingPermissions(list(utils.PermissionChecker.MUTE_PERMS))

        if self.is_muted(member()):
            raise commands.UserInputError()
        else:
            await self.do_mute(ctx, member())

    @commands.command(aliases=['размут'])
    @commands.guild_only()
    async def unmute(self, ctx: commands.Context, member: utils.MemberConverter):
        """Убирает мут у пользователя.
        member - ID, упоминание или никнейм (если содержит пробелы, должен быть заключен в кавычки).
        Необходимо разрешение управлять ролями для бота и вызывающего команду.
        Для информации о работе команды см. команду mute."""

        if utils.PermissionChecker.compare(ctx.author, member()) <= 0 \
                or not utils.PermissionChecker.is_able_to_mute(ctx.author):
            raise commands.MissingPermissions(list(utils.PermissionChecker.MUTE_PERMS))

        if utils.PermissionChecker.compare(ctx.me, member()) <= 0 \
                or not utils.PermissionChecker.is_able_to_mute(ctx.me):
            raise commands.BotMissingPermissions(list(utils.PermissionChecker.MUTE_PERMS))

        if self.is_muted(member()):
            await self.do_unmute(ctx, member())
        else:
            raise commands.UserInputError()

    async def do_mute(self, ctx: commands.Context, member: discord.Member, unmute_delay: datetime.timedelta = None):
        await self.update_role(member.guild)
        if self.is_muted(member): return
        await self.add_role(member)
        self.add_record(member, unmute_delay)
        await self.add_db_record(member, unmute_delay)
        await ctx.send(':zipper_mouth: Мут добавлен пользователю %s' % member)

    async def do_unmute(self, ctx: commands.Context, member: discord.Member):
        await self.do_unmute_by_id(member.guild.id, member.id)
        await ctx.send(':zipper_mouth: Мут снят с пользователя %s' % member)

    async def do_unmute_by_id(self, guild_id: int, user_id: int):
        guild = self.bot.get_guild(guild_id)
        member = guild.get_member(user_id) if guild else None
        if member is None or not self.is_muted(member): return
        await self.remove_role(member)
        self.remove_record(member)
        await self.remove_db_record(member)

    def is_muted(self, member: discord.Member) -> bool:
        muted_role = self.get_role(member.guild)
        return muted_role in member.roles

    def get_role(self, guild: discord.Guild) -> typing.Optional[discord.Role]:
        settings = self.bot.get_cog('SettingsCog')[guild]
        return next(filter(lambda r: r.name == settings.muted_role, guild.roles), None)

    async def create_role(self, guild: discord.Guild) -> discord.Role:
        settings = self.bot.get_cog('SettingsCog')[guild]
        return await guild.create_role(name=settings.muted_role, color=discord.Color.red())

    async def update_role(self, guild: discord.Guild):
        muted_role = self.get_role(guild)
        if muted_role is None: muted_role = await self.create_role(guild)
        updated_permissions = {'add_reactions': False, 'send_messages': False, 'speak': False}
        tasks = [channel.set_permissions(muted_role, **updated_permissions) for channel in guild.channels]
        await asyncio.wait(tasks)

    async def add_role(self, member: discord.Member):
        muted_role = self.get_role(member.guild)
        await member.add_roles(muted_role, reason='Мут (Blocker)')

    async def remove_role(self, member: discord.Member):
        muted_role = self.get_role(member.guild)
        await member.remove_roles(muted_role, reason='Мут (Blocker)')

    def add_record(self, member: discord.Member, unmute_delay: datetime.timedelta = None):
        mute = next(filter(lambda m: m.guild_id == member.guild.id and m.user_id == member.id, self.__mutes), None)

        if mute is not None:
            mute.cancel()

        self.__mutes.append(Mute(member=member, unmute_delay=unmute_delay, unmute_action=self.do_unmute_by_id))

    def remove_record(self, member: discord.Member):
        mute = next(filter(lambda m: m.guild_id == member.guild.id and m.user_id == member.id, self.__mutes), None)

        if mute is not None:
            mute.cancel()
            self.__mutes.remove(mute)

    async def add_db_record(self, member: discord.Member, unmute_delay: datetime.timedelta = None):
        unmute_date = datetime.datetime.now() + unmute_delay if unmute_delay is not None else None
        search = {'guild_id': member.guild.id, 'user_id': member.id}
        update = {'unmute_date': unmute_date}
        await db_client.DbClient().get_database()['mutes'].update_one(search, {'$set': update}, upsert=True)

    async def remove_db_record(self, member: discord.Member):
        search = {'guild_id': member.guild.id, 'user_id': member.id}
        await db_client.DbClient().get_database()['mutes'].delete_one(search)
