import logging
import typing

import discord
from discord.ext import commands

from blocker import utils


class HelpCommand(commands.HelpCommand):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def get_cog_name(self, cog: commands.Cog) -> str:
        return cog.qualified_name if cog is not None else self.no_category

    def get_commands(self, cmds: typing.List[commands.Command]) -> str:
        result = ''

        for cmd in cmds:
            if len(cmd.aliases) > 0:
                aliases = '/'.join(cmd.aliases)
                result += '`{}/{}` - {}\n'.format(cmd.name, aliases, cmd.short_doc)
            else:
                result += '`{}` - {}\n'.format(cmd.name, cmd.short_doc)

        return result

    @property
    def no_category(self) -> str:
        return 'DefaultCog'

    @property
    def bot_heading(self) -> str:
        return 'Справка'

    @property
    def commands_heading(self) -> str:
        return 'Команды'

    @property
    def ending_note(self) -> str:
        return 'Используй "{0}help команда", чтобы получить информацию о команде ' \
               'или "{0}help категория", чтобы получить больше информации о категории.' \
            .format(self.context.bot.command_prefix)

    async def send_bot_help(self, mapping: typing.Dict[typing.Optional[commands.Cog], typing.List[commands.Command]]):
        embed_title = self.bot_heading
        embed_description = self.context.bot.description
        embed = discord.Embed(title=embed_title, description=embed_description, color=discord.Color.blue())

        for cog, cmds in mapping.items():
            field_title = self.get_cog_name(cog)
            field_description = self.get_commands(cmds)
            embed.add_field(name=field_title, value=field_description)

        embed.set_footer(text=self.ending_note)
        await self.get_destination().send(embed=embed)

    async def send_cog_help(self, cog: commands.Cog):
        embed_title = self.get_cog_name(cog)
        embed = discord.Embed(title=embed_title, color=discord.Color.blue())

        cmds_field_title = self.commands_heading
        cmds_field_description = self.get_commands(cog.get_commands())
        embed.add_field(name=cmds_field_title, value=cmds_field_description)

        embed.set_footer(text=self.ending_note)
        await self.get_destination().send(embed=embed)

    async def send_command_help(self, command: commands.Command):
        embed_title = self.get_command_signature(command)
        embed_description = command.help
        embed = discord.Embed(title=embed_title, description=embed_description, color=discord.Color.blue())

        await self.get_destination().send(embed=embed)


class Bot(commands.Bot, metaclass=utils.Singleton):
    messages: int

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.help_command = HelpCommand(command_attrs={'aliases': ['справка']})
        self.messages = 0

    async def on_ready(self):
        logging.getLogger('blocker').info('Вошёл как %s', self.user)

    async def on_message(self, msg: discord.Message):
        if msg.author.id == self.user.id: return
        self.messages += 1
        logging.getLogger('blocker').info('Получено сообщение от %s', msg.author)
        await self.process_commands(msg)

    async def on_command_error(self, ctx: commands.Context, err: commands.CommandError):
        if isinstance(err, commands.CommandNotFound):
            pass
        elif isinstance(err, commands.UserInputError):
            if isinstance(err, commands.MissingRequiredArgument):
                await ctx.send(utils.BlockerError('Чего то не хватает, проверь, ничего ли ты не забыл :)'))
            else:
                await ctx.send(utils.BlockerError('Что-то тут не так, проверь, пожалуйста ^-^'))
        elif isinstance(err, commands.CommandInvokeError) and isinstance(err.original, utils.BlockerError):
            await ctx.send(err.original)
        elif isinstance(err, commands.CheckFailure):
            if isinstance(err, commands.MissingPermissions):
                await ctx.send(utils.BlockerError('Упс, ты не можешь этого сделать'))
            elif isinstance(err, commands.BotMissingPermissions):
                await ctx.send(utils.BlockerError('Кхм, кажется, я не могу этого сделать'))
            else:
                pass
        else:
            raise err
