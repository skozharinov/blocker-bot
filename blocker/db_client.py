from motor import motor_asyncio

from blocker import utils


class DbClient(motor_asyncio.AsyncIOMotorClient, metaclass=utils.Singleton):
    pass
