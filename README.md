# Blocker
Russian bot designed to simplify moderation of Discord servers.

### Requirements
* Python 3.6+

### Preparation
```bash
pip install -r requirements.txt
```

### Usage
```bash
python3 -m blocker -D <your Discord API token here> -M <your MongoDB URI here>
```

Then generate an invite link for bot with following permissions:
* Manage roles
* Manage channels
* Kick members
* Ban members
* Manage nicknames
* Read messages
* Send messages
* Manage messages
* Read message history

Start using the bot with '!help' command.
